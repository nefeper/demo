# Ejercicio

El objetivo de este ejercicio es conocer cómo los candidatos a entrar a
almundo.com usan herramientas básicas de Java y diseñan soluciones
orientadas a objetos.

## Consigna

Existe un call center donde hay 3 tipos de empleados: operador, supervisor
y director. El proceso de la atención de una llamada telefónica en primera
instancia debe ser atendida por un operador, si no hay ninguno libre debe
ser atendida por un supervisor, y de no haber tampoco supervisores libres
debe ser atendida por un director.


## Requerimientos

- Debe existir una clase Dispatcher encargada de manejar las llamadas, y debe contener el método dispatchCall para que las asigne a los empleados disponibles.
- El método dispatchCall puede invocarse por varios hilos al mismo tiempo.
- La clase Dispatcher debe tener la capacidad de poder procesar 10 llamadas al mismo tiempo (de modo concurrente). 
- Cada llamada puede durar un tiempo aleatorio entre 5 y 10 segundos.
- Debe tener un test unitario donde lleguen 10 llamadas.

## Extras/Plus

- Dar alguna solución sobre qué pasa con una llamada cuando no hay ningún empleado libre.
- Dar alguna solución sobre qué pasa con una llamada cuando entran más de 10 llamadas concurrentes.
- Agregar los tests unitarios que se crean convenientes.
- Agregar documentación de código.


## Tener en Cuenta

- El proyecto debe ser creado con Maven.
- De ser necesario, anexar un documento con la explicación del cómo y porqué resolvió los puntos extras, o comentarlo en las clases donde se encuentran sus tests unitarios respectivos.


## Documentación

Se realizó una asignación de responsabilidades a las clases, funciones y paquetes, para ello también se utilizaron conceptos de patrones de diseño tales:

## Patrones de Diseños Utilizados
- **Value Object** utilizado para manejar la información de los empleados y llamadas.
- **SimpleFactory** utilizado para crear los objetos de las clases de InfoLlamadaVO y InfoEmpleadoVO.
- **Observer** se utilizó conceptos de este patrón para notificar las llamadas recibidas por dispatcher "**Subject**" a los empleados "**Observador**" disponibles.   

## Comentarios Generales

- Se creó una clase main "**co.com.almundo.callcenter.app.App**" para ejecutar el proyecto.

- Se creó una clase para manejar las constantes del proyecto.

- Se implementaron con las siguientes interfaces **java.util.concurrent.ExecutorService** y **java.lang.Runnable**, la forma de administrar y crear los hilos.

- Se utilizó una Cola "**java.util.Queue**", con la implementación **java.util.ConcurrentLinkedQueue** para desarrollar los puntos extras cuando no hay empleados disponibles y cuando lleguen más de 10 llamadas, la motivación de esta selección es por su concurrencia.

- Se orientó el desarrollo basado en interfaces como buena práctica de desarrollo, creando y utilizando interfaces para tener un bajo acoplamiento. Ej: "Queue,List"... "Dispatcher,EmpleadoObservador,Usuario"

- En el estándar de nombramiento en las implementaciones de las interfaces se utilizó el sufijo "**Impl**", para encapsular la información de llamada y empleados se utilizó "**Vo**" relacionado al patrón Value Object.  



# Diagramas 

## 1. Diagrama de Clases

![alt text](src/main/docs/imagenes/ModeloClases.png "Diagrama de Clases Call Center")

## 2. Diagrama de Secuencia

![alt text](src/main/docs/imagenes/DiagramaSecuencia.png "Diagrama de Secuencia Call Center")
