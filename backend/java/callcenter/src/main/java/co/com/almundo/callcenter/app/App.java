package co.com.almundo.callcenter.app;

import co.com.almundo.callcenter.executor.Executor;

public class App {

  private App() {
  }

  public static void main(String[] args) throws InterruptedException {
    new Executor().run();
  }

}
