package co.com.almundo.callcenter.constants;

public final class Constantes {

  
  public static final int NUM_TELEFONOS_MIN = 1000;
  public static final int NUM_TELEFONOS_MAX = 9999;
  public static final String PREFIJO_TELEFONO = "555";

  public static final int NUMERO_EMPLEADOS = 10;

  public static final int MIN_USUARIOS = 10;
  public static final int MAX_USUARIOS = 30;

  public static final int TIEMPO_MIN = 5000;
  public static final int TIEMPO_MAX = 10000;

  private Constantes() {
  }

}
