package co.com.almundo.callcenter.enums;

public enum EstadoEmpleado {
  DISPONIBLE(0), OCUPADO(1);

  private final int codigo;

  EstadoEmpleado(int codigo) {
    this.codigo = codigo;
  }

  public int getCodigo() {
    return codigo;
  }

}
