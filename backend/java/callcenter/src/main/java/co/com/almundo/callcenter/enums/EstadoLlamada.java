package co.com.almundo.callcenter.enums;

public enum EstadoLlamada {
  MARCANDO(1), ENESPERA(2), CONVERSANDO(3), FINALIZADA(4);

  private final int codigo;

  EstadoLlamada(int codigo) {
    this.codigo = codigo;
  }

  public int getCodigo() {
    return codigo;
  }
}
