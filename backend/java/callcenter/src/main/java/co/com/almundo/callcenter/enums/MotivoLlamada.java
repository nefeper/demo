package co.com.almundo.callcenter.enums;

public enum MotivoLlamada {
  COTIZACION(1), RESERVA(2), PAGO(3), CAMBIO(4);

  private final int codigo;

  MotivoLlamada(int codigo) {
    this.codigo = codigo;
  }

  public int getCodigo() {
    return codigo;
  }

  public static MotivoLlamada valueOfCodigo(int codigo) {
    for (MotivoLlamada motivo : MotivoLlamada.values()) {
      if (motivo.codigo == codigo) {
        return motivo;
      }
    }
    return MotivoLlamada.COTIZACION;
  }
}
