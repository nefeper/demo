package co.com.almundo.callcenter.enums;

public enum TipoEmpleado {
  OPERADOR(1), SUPERVISOR(2), DIRECTOR(3);

  private final int codigo;

  TipoEmpleado(int codigo) {
    this.codigo = codigo;
  }

  public int getCodigo() {
    return codigo;
  }
}
