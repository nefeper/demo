package co.com.almundo.callcenter.executor;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.almundo.callcenter.constants.Constantes;
import co.com.almundo.callcenter.factory.SimpleFactoryVo;
import co.com.almundo.callcenter.runnables.Dispatcher;
import co.com.almundo.callcenter.runnables.DispatcherImpl;
import co.com.almundo.callcenter.runnables.EmpleadoObservador;
import co.com.almundo.callcenter.runnables.EmpleadoObservadorImpl;
import co.com.almundo.callcenter.runnables.UsuarioImpl;
import co.com.almundo.callcenter.vo.InfoEmpleadoVo;

public class Executor {

  private static final Logger LOG = LoggerFactory.getLogger(Executor.class);

  public void run() throws InterruptedException {
    int numeroLLamadas = ThreadLocalRandom.current().nextInt(Constantes.MIN_USUARIOS, Constantes.MAX_USUARIOS + 1);
    ExecutorService executorDispatcher = Executors.newSingleThreadExecutor();
    ExecutorService executor = Executors.newFixedThreadPool(Constantes.NUMERO_EMPLEADOS + numeroLLamadas);

    Dispatcher dispatcher = new DispatcherImpl();
    executorDispatcher.execute(dispatcher);

    List<InfoEmpleadoVo> list = SimpleFactoryVo.crearInformacionEmpleados();
    for (InfoEmpleadoVo empleadoVo : list) {
      EmpleadoObservador empleado = new EmpleadoObservadorImpl(empleadoVo);
      dispatcher.addEmpleadoObservador(empleado);
      executor.execute(empleado);
    }

    LOG.info("numero de llamadas sera : " + numeroLLamadas);
    for (int i = 1; i <= numeroLLamadas; i++) {
      executor.execute(new UsuarioImpl(dispatcher));
    }

    executor.shutdown();
    executorDispatcher.shutdown();

    executor.awaitTermination((numeroLLamadas / Constantes.NUMERO_EMPLEADOS) * Constantes.TIEMPO_MAX, TimeUnit.SECONDS);
    executorDispatcher.awaitTermination((numeroLLamadas / Constantes.NUMERO_EMPLEADOS) * Constantes.TIEMPO_MAX, TimeUnit.SECONDS);
  }

}
