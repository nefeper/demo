package co.com.almundo.callcenter.factory;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import co.com.almundo.callcenter.constants.Constantes;
import co.com.almundo.callcenter.enums.EstadoEmpleado;
import co.com.almundo.callcenter.enums.EstadoLlamada;
import co.com.almundo.callcenter.enums.MotivoLlamada;
import co.com.almundo.callcenter.enums.TipoEmpleado;
import co.com.almundo.callcenter.vo.InfoEmpleadoVo;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public class SimpleFactoryVo {

  private static AtomicInteger idLlamada = new AtomicInteger(1);
  private static AtomicInteger idEmpleado = new AtomicInteger(1);

  private SimpleFactoryVo() {
  }

  public static InfoLlamadaVo crearDatoLlamada() {
    InfoLlamadaVo datoLlamada = new InfoLlamadaVo();
    datoLlamada.setId(idLlamada.getAndIncrement());
    int telefono = ThreadLocalRandom.current().nextInt(Constantes.NUM_TELEFONOS_MIN, Constantes.NUM_TELEFONOS_MAX + 1);
    datoLlamada.setTelefono(Constantes.PREFIJO_TELEFONO + "-" + telefono);
    datoLlamada.setEstado(EstadoLlamada.MARCANDO);
    datoLlamada.setMotivo(MotivoLlamada.valueOfCodigo(new Random().nextInt(MotivoLlamada.values().length + 1)));
    return datoLlamada;
  }

  public static InfoEmpleadoVo crearInformacionOperador() {
    InfoEmpleadoVo empleadoVo = new InfoEmpleadoVo();
    empleadoVo.setId(idEmpleado.getAndIncrement());
    empleadoVo.setTipo(TipoEmpleado.OPERADOR);
    empleadoVo.setEstado(EstadoEmpleado.DISPONIBLE);
    return empleadoVo;
  }

  public static InfoEmpleadoVo crearInformacionSupervisor() {
    InfoEmpleadoVo empleadoVo = new InfoEmpleadoVo();
    empleadoVo.setId(idEmpleado.getAndIncrement());
    empleadoVo.setTipo(TipoEmpleado.SUPERVISOR);
    empleadoVo.setEstado(EstadoEmpleado.DISPONIBLE);
    return empleadoVo;
  }

  public static InfoEmpleadoVo crearInformacionDirector() {
    InfoEmpleadoVo empleadoVo = new InfoEmpleadoVo();
    empleadoVo.setId(idEmpleado.getAndIncrement());
    empleadoVo.setTipo(TipoEmpleado.DIRECTOR);
    empleadoVo.setEstado(EstadoEmpleado.DISPONIBLE);
    return empleadoVo;
  }

  public static List<InfoEmpleadoVo> crearInformacionEmpleados() {
    return Arrays.asList(SimpleFactoryVo.crearInformacionOperador(), SimpleFactoryVo.crearInformacionOperador(), SimpleFactoryVo.crearInformacionOperador(), SimpleFactoryVo.crearInformacionOperador(), SimpleFactoryVo.crearInformacionOperador(),
        SimpleFactoryVo.crearInformacionOperador(), SimpleFactoryVo.crearInformacionSupervisor(), SimpleFactoryVo.crearInformacionSupervisor(), SimpleFactoryVo.crearInformacionDirector(), SimpleFactoryVo.crearInformacionDirector());
  }

}
