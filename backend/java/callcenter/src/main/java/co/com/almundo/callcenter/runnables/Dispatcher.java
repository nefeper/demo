package co.com.almundo.callcenter.runnables;

import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public interface Dispatcher extends Runnable {

  void addEmpleadoObservador(EmpleadoObservador empleado);

  void dispatchCall(InfoLlamadaVo llamada);

  void asignarLlamada();

}
