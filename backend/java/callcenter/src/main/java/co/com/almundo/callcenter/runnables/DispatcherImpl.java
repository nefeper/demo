package co.com.almundo.callcenter.runnables;

import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.almundo.callcenter.enums.EstadoEmpleado;
import co.com.almundo.callcenter.enums.EstadoLlamada;
import co.com.almundo.callcenter.enums.TipoEmpleado;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public class DispatcherImpl implements Dispatcher {

  private static final Logger LOG = LoggerFactory.getLogger(DispatcherImpl.class);

  private Boolean activo = true;

  private Queue<InfoLlamadaVo> llamadas;

  private Queue<EmpleadoObservador> empleados;

  public DispatcherImpl() {
    super();
    llamadas = new ConcurrentLinkedQueue<>();
    empleados = new ConcurrentLinkedQueue<>();
  }

  private EmpleadoObservador buscarEmpleadoDisponible() {
    List<EmpleadoObservador> disponible = empleados.stream().filter(e -> e.getInfoEmpleado().getEstado() == EstadoEmpleado.DISPONIBLE).collect(Collectors.toList());
    Optional<EmpleadoObservador> empleado = disponible.stream().filter(e -> e.getInfoEmpleado().getTipo() == TipoEmpleado.OPERADOR).findAny();
    if (!empleado.isPresent()) {
      empleado = disponible.stream().filter(e -> e.getInfoEmpleado().getTipo() == TipoEmpleado.SUPERVISOR).findAny();
      if (!empleado.isPresent()) {
        empleado = disponible.stream().filter(e -> e.getInfoEmpleado().getTipo() == TipoEmpleado.DIRECTOR).findAny();
      }
    }
    return empleado.isPresent() ? empleado.get() : null;
  }

  @Override
  public void addEmpleadoObservador(EmpleadoObservador empleado) {
    empleados.add(empleado);
  }

  @Override
  public void dispatchCall(InfoLlamadaVo llamada) {
    llamada.setEstado(EstadoLlamada.ENESPERA);
    LOG.info("Entrando llamada {} al Dispatcher: {} ", llamada.getId(), llamada.toString());
    this.llamadas.add(llamada);
  }

  @Override
  public void asignarLlamada() {
    EmpleadoObservador empleado = buscarEmpleadoDisponible();
    if (empleado != null) {
      empleado.getInfoEmpleado().setEstado(EstadoEmpleado.OCUPADO);
      empleado.recibirLlamada(this.llamadas.poll());
    }
  }

  @Override
  public void run() {
    while (activo) {
      if (!this.llamadas.isEmpty()) {
        asignarLlamada();
      }
    }
  }

}
