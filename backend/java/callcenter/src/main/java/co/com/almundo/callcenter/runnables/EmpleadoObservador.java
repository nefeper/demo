package co.com.almundo.callcenter.runnables;

import co.com.almundo.callcenter.vo.InfoEmpleadoVo;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public interface EmpleadoObservador extends Runnable {
  
  InfoEmpleadoVo getInfoEmpleado();

  void setInfoEmpleado(InfoEmpleadoVo infoEmpleado);

  void recibirLlamada(InfoLlamadaVo llamada);

  void contestarLlamada(InfoLlamadaVo llamada);

}