package co.com.almundo.callcenter.runnables;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.almundo.callcenter.constants.Constantes;
import co.com.almundo.callcenter.enums.EstadoEmpleado;
import co.com.almundo.callcenter.enums.EstadoLlamada;
import co.com.almundo.callcenter.vo.InfoEmpleadoVo;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public class EmpleadoObservadorImpl implements EmpleadoObservador {

  private static final Logger LOG = LoggerFactory.getLogger(EmpleadoObservadorImpl.class);

  private Boolean activo = true;
  private InfoEmpleadoVo infoEmpleado;
  private Queue<InfoLlamadaVo> llamadas;

  public EmpleadoObservadorImpl() {
    this.llamadas = new ConcurrentLinkedQueue<>();
  }

  public EmpleadoObservadorImpl(InfoEmpleadoVo infoEmpleado) {
    this.infoEmpleado = infoEmpleado;
    this.llamadas = new ConcurrentLinkedQueue<>();
  }

  @Override
  public InfoEmpleadoVo getInfoEmpleado() {
    return this.infoEmpleado;
  }

  @Override
  public void setInfoEmpleado(InfoEmpleadoVo infoEmpleado) {
    this.infoEmpleado = infoEmpleado;
  }

  @Override
  public void recibirLlamada(InfoLlamadaVo llamada) {
    LOG.info("Llamada {} asignada al empleado {},  datos : {} {} ", llamada.getId(), infoEmpleado.getId(), infoEmpleado.toString(), llamada.toString());
    this.llamadas.add(llamada);
  }

  @Override
  public void contestarLlamada(InfoLlamadaVo llamada) {
    try {
      llamada.setEstado(EstadoLlamada.CONVERSANDO);
      llamada.setTiempo(ThreadLocalRandom.current().nextInt(Constantes.TIEMPO_MIN, Constantes.TIEMPO_MAX + 1));
      LOG.info("Atendiendo Llamada {} asignada al empleado {},  datos : {} {} ", llamada.getId(), infoEmpleado.getId(), infoEmpleado.toString(), llamada.toString());
      Thread.sleep(llamada.getTiempo());
    } catch (InterruptedException e) {
      LOG.error(e.getMessage());
      Thread.currentThread().interrupt();
    } finally {
      llamada.setEstado(EstadoLlamada.FINALIZADA);
      infoEmpleado.setEstado(EstadoEmpleado.DISPONIBLE);
      LOG.info("Finalizando Llamada {} asignada al empleado {},  datos : {} {} ", llamada.getId(), infoEmpleado.getId(), infoEmpleado.toString(), llamada.toString());
    }
  }

  @Override
  public void run() {
    while (activo) {
      if (!this.llamadas.isEmpty()) {
        contestarLlamada(this.llamadas.poll());
      }
    }
  }

}
