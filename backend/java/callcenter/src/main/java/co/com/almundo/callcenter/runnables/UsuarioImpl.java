package co.com.almundo.callcenter.runnables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.com.almundo.callcenter.factory.SimpleFactoryVo;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public class UsuarioImpl implements Usuario {

  private static final Logger LOG = LoggerFactory.getLogger(UsuarioImpl.class);

  private Dispatcher dispatcher;

  public UsuarioImpl(Dispatcher dispatcher) {
    this.dispatcher = dispatcher;
  }

  @Override
  public void marcando() {
    InfoLlamadaVo llamada = SimpleFactoryVo.crearDatoLlamada();
    LOG.info("Marcando Llamada {} : {} ", llamada.getId(), llamada.toString());
    dispatcher.dispatchCall(llamada);
  }

  @Override
  public void run() {
    marcando();
  }

}
