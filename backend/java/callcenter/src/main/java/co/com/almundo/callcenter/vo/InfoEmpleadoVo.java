package co.com.almundo.callcenter.vo;

import java.io.Serializable;

import co.com.almundo.callcenter.enums.EstadoEmpleado;
import co.com.almundo.callcenter.enums.TipoEmpleado;

public class InfoEmpleadoVo implements Serializable {

  private static final long serialVersionUID = -1779744262152810667L;
  private int id;
  private TipoEmpleado tipo;
  private EstadoEmpleado estado;

  public InfoEmpleadoVo() {
    super();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public TipoEmpleado getTipo() {
    return tipo;
  }

  public void setTipo(TipoEmpleado tipo) {
    this.tipo = tipo;
  }

  public EstadoEmpleado getEstado() {
    return estado;
  }

  public void setEstado(EstadoEmpleado estado) {
    this.estado = estado;
  }

  @Override
  public String toString() {
    return "InfoEmpleadoVo [id=" + id + ", tipo=" + tipo + ", estado=" + estado + "]";
  }

}
