package co.com.almundo.callcenter.vo;

import java.io.Serializable;

import co.com.almundo.callcenter.enums.EstadoLlamada;
import co.com.almundo.callcenter.enums.MotivoLlamada;

public class InfoLlamadaVo implements Serializable {

  private static final long serialVersionUID = 4565856850410763312L;

  private int id;
  private String telefono;
  private int tiempo;
  private EstadoLlamada estado;
  private MotivoLlamada motivo;

  public InfoLlamadaVo() {
    super();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public int getTiempo() {
    return tiempo;
  }

  public void setTiempo(int tiempo) {
    this.tiempo = tiempo;
  }

  public EstadoLlamada getEstado() {
    return estado;
  }

  public void setEstado(EstadoLlamada estado) {
    this.estado = estado;
  }

  public MotivoLlamada getMotivo() {
    return motivo;
  }

  public void setMotivo(MotivoLlamada motivo) {
    this.motivo = motivo;
  }

  @Override
  public String toString() {
    return "InfoLlamadaVo [id=" + id + ", telefono=" + telefono + ", tiempo=" + tiempo / 1000 + ", estado=" + estado + ", motivo=" + motivo + "]";
  }

}