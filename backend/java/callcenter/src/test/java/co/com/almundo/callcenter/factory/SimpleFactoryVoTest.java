package co.com.almundo.callcenter.factory;

import org.junit.Assert;
import org.junit.Test;

import co.com.almundo.callcenter.enums.EstadoEmpleado;
import co.com.almundo.callcenter.enums.EstadoLlamada;
import co.com.almundo.callcenter.enums.TipoEmpleado;
import co.com.almundo.callcenter.vo.InfoEmpleadoVo;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public class SimpleFactoryVoTest {

  @Test
  public void crearDatoLlamada() {
    InfoLlamadaVo llamada = SimpleFactoryVo.crearDatoLlamada();
    Assert.assertTrue(llamada.getId() > 0);
    Assert.assertEquals(llamada.getEstado(), EstadoLlamada.MARCANDO);
    Assert.assertEquals(llamada.getTiempo(), 0);
  }

  @Test
  public void crearInformacionOperador() {
    InfoEmpleadoVo empleado = SimpleFactoryVo.crearInformacionOperador();
    Assert.assertTrue(empleado.getId() > 0);
    Assert.assertEquals(empleado.getEstado(), EstadoEmpleado.DISPONIBLE);
    Assert.assertEquals(empleado.getTipo(), TipoEmpleado.OPERADOR);
  }

  @Test
  public void crearInformacionSupervisor() {
    InfoEmpleadoVo empleado = SimpleFactoryVo.crearInformacionOperador();
    Assert.assertTrue(empleado.getId() > 0);
    Assert.assertEquals(empleado.getEstado(), EstadoEmpleado.DISPONIBLE);
    Assert.assertEquals(empleado.getTipo(), TipoEmpleado.SUPERVISOR);
  }

  @Test
  public void crearInformacionDirector() {
    InfoEmpleadoVo empleado = SimpleFactoryVo.crearInformacionOperador();
    Assert.assertTrue(empleado.getId() > 0);
    Assert.assertEquals(empleado.getEstado(), EstadoEmpleado.DISPONIBLE);
    Assert.assertEquals(empleado.getTipo(), TipoEmpleado.DIRECTOR);
  }

}
