package co.com.almundo.callcenter.runnables;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import co.com.almundo.callcenter.enums.EstadoEmpleado;
import co.com.almundo.callcenter.enums.EstadoLlamada;
import co.com.almundo.callcenter.factory.SimpleFactoryVo;
import co.com.almundo.callcenter.vo.InfoEmpleadoVo;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DispatcherImpl.class)
public class DispatcherTest {

  @Test
  public void run() throws Exception {
    DispatcherImpl dispatcher = PowerMockito.spy(new DispatcherImpl());
    EmpleadoObservadorImpl empleadoObservador = PowerMockito.mock(EmpleadoObservadorImpl.class);

    InfoEmpleadoVo empleado = SimpleFactoryVo.crearInformacionOperador();
    InfoLlamadaVo llamada = SimpleFactoryVo.crearDatoLlamada();
    
    empleadoObservador.setInfoEmpleado(empleado);
    dispatcher.addEmpleadoObservador(empleadoObservador);

    PowerMockito.when(empleadoObservador.getInfoEmpleado()).thenReturn(empleado);

    Assert.assertEquals(llamada.getEstado(), EstadoLlamada.MARCANDO);
    Assert.assertEquals(empleadoObservador.getInfoEmpleado().getEstado(), EstadoEmpleado.DISPONIBLE);
    
    dispatcher.dispatchCall(llamada);
    dispatcher.run();
    
    Mockito.verify(dispatcher).asignarLlamada();
    PowerMockito.verifyPrivate(dispatcher).invoke(PowerMockito.method(DispatcherImpl.class, "buscarEmpleadoDisponible"));
    Mockito.verify(empleadoObservador).recibirLlamada(Mockito.any(InfoLlamadaVo.class));
    Assert.assertEquals(empleadoObservador.getInfoEmpleado().getEstado(), EstadoEmpleado.OCUPADO);
    Assert.assertEquals(llamada.getEstado(), EstadoLlamada.ENESPERA);
  }

}
