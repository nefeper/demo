package co.com.almundo.callcenter.runnables;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import co.com.almundo.callcenter.enums.EstadoEmpleado;
import co.com.almundo.callcenter.enums.EstadoLlamada;
import co.com.almundo.callcenter.factory.SimpleFactoryVo;
import co.com.almundo.callcenter.vo.InfoEmpleadoVo;
import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public class EmpleadoObservadorTest {

  @Test
  public void run() {
    EmpleadoObservadorImpl empleadoObservador = Mockito.spy(new EmpleadoObservadorImpl());

    InfoEmpleadoVo empleado = SimpleFactoryVo.crearInformacionOperador();
    InfoLlamadaVo llamada = SimpleFactoryVo.crearDatoLlamada();

    empleadoObservador.setInfoEmpleado(empleado);

    empleado.setEstado(EstadoEmpleado.OCUPADO);

    Assert.assertTrue(llamada.getTiempo() == 0);
    Assert.assertEquals(llamada.getEstado(), EstadoLlamada.MARCANDO);
    Assert.assertEquals(empleado.getEstado(), EstadoEmpleado.OCUPADO);

    empleadoObservador.recibirLlamada(llamada);
    empleadoObservador.run();

    Mockito.verify(empleadoObservador).contestarLlamada(Mockito.any(InfoLlamadaVo.class));

    Assert.assertEquals(empleado.getEstado(), EstadoEmpleado.DISPONIBLE);
    Assert.assertEquals(llamada.getEstado(), EstadoLlamada.FINALIZADA);
    Assert.assertTrue(llamada.getTiempo() > 0);
  }

}
