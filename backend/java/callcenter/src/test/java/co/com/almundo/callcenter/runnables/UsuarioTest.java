package co.com.almundo.callcenter.runnables;

import org.junit.Test;
import org.mockito.Mockito;

import co.com.almundo.callcenter.vo.InfoLlamadaVo;

public class UsuarioTest {

  @Test
  public void marcando() {
    Dispatcher dispatcher = Mockito.mock(Dispatcher.class);
    UsuarioImpl usuario = Mockito.spy(new UsuarioImpl(dispatcher));
    usuario.run();
    Mockito.verify(usuario).marcando();
    Mockito.verify(dispatcher).dispatchCall(Mockito.any(InfoLlamadaVo.class));
  }

}
